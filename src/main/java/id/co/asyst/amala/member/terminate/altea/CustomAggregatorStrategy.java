package id.co.asyst.amala.member.terminate.altea;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

/**
 * @author Ahmad Dimas Abid Muttaqi
 * @version $Revision$, Apr 29, 2019
 * @since 1.0
 */
public class CustomAggregatorStrategy implements AggregationStrategy {

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        return newExchange;
    }
}