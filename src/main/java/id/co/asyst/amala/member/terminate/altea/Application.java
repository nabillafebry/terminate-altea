package id.co.asyst.amala.member.terminate.altea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource({"terminate-altea.xml", "beans.xml"})
public class Application {

    public static void main(String[] args) {

        SpringApplication.run(Application.class, args);
    }
}