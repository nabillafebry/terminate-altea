package id.co.asyst.amala.member.terminate.altea;

import com.google.gson.Gson;
import org.apache.camel.Exchange;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class Utils {

    private static Logger log = LogManager.getLogger(Utils.class);

    public void validateRequest(Exchange exchange) {
        Map<String, Object> requestdata = exchange.getProperty("requestdata", Map.class);

        String[] properties = new String[]{"startdate", "enddate", "updatedby"};
        String message = validate(requestdata, properties);
        if (message != null) {
            exchange.setProperty("resmsg", message);
            exchange.setProperty("rescode", "9005");
            exchange.setProperty("resdesc", "Invalid Data");
            return;
        }

        exchange.setProperty("startdate", requestdata.get("startdate"));
        exchange.setProperty("enddate", requestdata.get("enddate"));
        exchange.setProperty("updatedby", requestdata.get("updatedby"));

    }

    public void setCDCStructure(Exchange exchange) {
        Map<String, Object> member = exchange.getProperty("profile", Map.class);

        String memberid = member.get("memberid").toString();
        String status = member.get("status").toString();

        Map<String, Object> before = new HashMap<>();
        before.put("memberid", memberid);
        before.put("status", "ACTIVE");

        Map<String, Object> after = new HashMap<>();
        after.put("memberid", memberid);
        after.put("status", status);

        Map<String, Object> source = new HashMap<>();
        source.put("table", "member");

        Map<String, Object> mapstructure = new HashMap<>();
        mapstructure.put("before", before);
        mapstructure.put("after", after);
        mapstructure.put("source", source);

        Gson gson = new Gson();

        exchange.getOut().setBody(gson.toJson(mapstructure));
    }


    private String validate(Map<String, Object> data, String property) {
        if (data.get(property) == null || data.get(property).toString().equals(""))
            return property + " is required";
        return null;
    }

    private String validate(Map<String, Object> data, String[] properties) {
        for (String property : properties) {
            if (data.get(property) == null || data.get(property).toString().equals("")) {
                return property + " is required";
            }


        }
        return null;
    }


}
